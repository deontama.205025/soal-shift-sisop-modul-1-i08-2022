
#!/bin/bash

check=1

echo "Please create username and password to register"
echo "CREATE USERNAME: "
read username

echo "CREATE PASSWORD: "
echo "Password must meet these criteria: " 
echo "1.Minumun 8 characters"
echo "2.Have at least 1 capital letter and 1 lowercase letter"
echo "3.Alphanumeric"
echo "4.Can't be the same as the username"

while [ $check == 1 ]
do
    read -s pass;

    if [ ${#pass} -lt 8 ]
    then 
        echo "Password too short"
    elif ! [[ "$pass" =~ [[:upper:]] ]] || ! [[ "$pass" =~ [[:lower:]] ]]
    then
        echo "Password must contain at least 1 capital and 1 lowercase letter"
    elif ! [[ "$pass" =~ ^[[:alnum:]]+$ ]]
    then
        echo "Password must contain alphanumeric"
    elif [[ "$pass" ==  "$username" ]]
    then 
        echo "Password cannot be the same as username"
    else 
        check=0
    fi
done

date=$(date +"%m/%d/%y %H:%M:%S")

if [[ -f register.txt ]]
then if grep -q "$username" register.txt
    then
        echo "username already exist"
        echo $date "REGISTER:ERROR user already exists" >> log.txt
    else
        echo "Registered user $username success"
        echo $date "REGISTER:INFO User $username registered successfully" >> log.txt
        echo "USER: $username PASS: $pass" >> register.txt
    fi
else
   echo "Registered user $username success" 
   echo $date "REGISTER:INFO User $username registered successfully" >> log.txt
   echo "USER: $username PASS: $pass" >> register.txt
fi
