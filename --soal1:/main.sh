#!/bin/bash

echo "PLEASE ENTER USERNAME AND PASSWORD TO LOGIN"
echo "ENTER USERNAME: "
read username
echo "ENTER PASSWORD: "
read pass

date=$(date +"%m/%d/%y %H:%M:%S")
image=dl
attr=att
dateDown=$(date +"%y-%m-%d")

if (grep -q "USER: $username PASS: $pass" register.txt)
then
   echo $date "LOGIN:INFO User $username logged in" >> log.txt 
   echo "type dl followed by total pictures you want to download ex: dl 2"
   echo "type att to display  the number of successfull or unsuccesful login attempts"
   read input
     
      if [[ "$input" == *"$image"* ]]
      then
         N=$(echo $input | awk '{print $2}')
         #echo $N
         format=$dateDown"_"$username
         #echo $format
         mkdir -p "$format"
         cd "$format"
    
         for((num=1;num<=$N;num++))
         do
            if [ $num -le 10 ]
            then 
               amn="0"
            else
               amn=""
            fi

            #curl {loremflickr.com} > PIC_${amn}${num}.jpg
            wget -O PIC_${amn}${num} https://loremflickr.com/320/240
            echo""
         done
  
        
         #wget https://loremflickr.com/320/240

      elif [[ "$input" == *"$atrr"* ]]
      then 
 
         logged=$(grep -o "LOGIN:INFO User $username logged in" log.txt | wc -l)
         failed=$(grep -o "LOGIN:ERROR Failed login attempt on user $username" log.txt | wc -l)
         echo "Successfull attempt: $logged, Unsuccessfull attempt: $failed."
      fi


else
   echo "Wrong Username or Password"
   echo $date "LOGIN:ERROR Failed login attempt on user $username" >> log.txt
fi
