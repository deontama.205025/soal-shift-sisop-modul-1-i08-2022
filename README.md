# soal-shift-sisop-modul-1-I08-2022

## REPORT GROUP I08 
| Nama		 		 | NRP		  | 
| :---         		 |     :---:      | 
| Deon Fitra Tama    | 5025201115     | 
| M. Akmal Riswanda  | 5025201143     | 
| Venia Sollery A H. | 5025201161	  |

## REPORT NO.1 

**Main.sh***

For the main script we are asked to make a login system using the username and password that have been registered. First the user have to insert their username and password. Then the program will check if the password is correct or not by using `grep -q` to check the password in the register file. 

`if grep -q '$pass' register.txt`

If the login is successfull it will log "LOGIN:INFO User username logged in" to the log file and if there is an unsuccessfull login attempt it wil log "LOGIN:ERROR Failed login attempt on user username". 

Once the user have logged in successfully they will be given an option to download an amount of images that they wish or to display the amount of successfull and unsuccessfull login attempt. 

```
echo "type dl followed by total pictures you want to download ex: dl 2"
echo "type att to display  the number of successfull or unsuccesful login attempts"
```

To download user have to type down dl and to display type down att. In order to match the user's input with dl or att we can use conditional operators and wildcard to match it. 

`if [[ "$input" == *"$image"* ]]`

To download the image with the wanted amount we use the for loop to get the wanted amount and use wget to download the images.

```
for((num=1;num<=$N;num++))
       do
         if [ $num -le 10 ]
         then 
         amn="0"
         else
         amn=""
         fi`
```

`wget -O PIC_${amn}${num} https://loremflickr.com/320/240`

To display the amount of successfull and unsuccessfull login attempts we can use the word count command. We also use `grep -o` to search the line that includes "LOGIN:INFO User $username logged in" and "LOGIN:ERROR Failed login attempt on user $username".
```
logged=$(grep -o "LOGIN:INFO User $username logged in" log.txt | wc -l)
failed=$(grep -o "LOGIN:ERROR Failed login attempt on user $username" log.txt | wc -l)
echo "Successfull attempt: $logged, Unsuccessfull attempt: $failed."
```
**Register.sh**

In this script we are told to make a register system where user create username and password.

User can choose any username they like, but for the password there are criteria such as:
1. Minumun 8 characters
1. Have at least 1 capital letter and 1 lowercase letter.
1. Alphanumeric
1. Can't be the same as the username

To make sure user satisfy all of the password's criteris we use the if else conditional statement.
This way the user's password will meet all the criteria.

Another thing we have to do is track the registration and login info into an txt file called log.txt.
To do this we will use the `>>` operational that is directed to log.txt. We will echo the date of the registration and status to log.txt. To get the current date we use date followed by the format that is asked by the problem.

```
echo $date "REGISTER:INFO User $username registered successfully" >> log.txt
```

This script also checks if a username already exist or not. To check this we can use `grep -q` to check it.

```
grep -q "$username" register.txt
```
If the username is taken the following information will be logged in the log.txt file.

```
echo $date "REGISTER:ERROR user already exists" >> log.txt
```

## REPORT NO.2 

In question number 2 there are several points from a-e where each point contains a different question. The explanation for each point is as follows:

### Question A 

create a folder called `forensic_log_website_daffainfo_log` 
here I use the command `$mkdir forensic_log_website_daffainfo_log` to create a new directory

### Question B 
What is the average hourly request that the attacker sends to website https://daffa.info/. in this case we are require to use awk syntax to solve the problem.
To solve the problem firstly i use a function:
`{split($0,a,":");b[a[2]":"a[3]"\""]++;}`
for spliting the index of the coloumns seperated by `:`. in this code i need to know from the index coloumn from "date" and "hour" to know how many request each hours . 
after that we just make a iteration from the new value of date and request which is the number of request in each hour. then count each request from each date then summarize the total and print. 
and then divide the total request with the number of each hours and print it.
```
for(i in b){
	 count++;
	 sum +=b[i];}
	 sum = sum/count;
	printf "Rata-rata serangan adalah sebanyak " sum " request perjam"
```
lastly the result will be past using `>`  into a file namely `rata-rata.txt`
therefore final source code can be writen as : 
```
awk '{split($0,a,":");
b[a[2]":"a[3]"\""]++;
}END{
	for(i in b){
	 count++;
	 sum +=b[i];}
	 sum = sum/count;
	printf "Rata-rata serangan adalah sebanyak " sum " request perjam"
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/rata-rata.txt 
```
### Question C 

In this point we need find the highest Ip address whom request to website and show how many times they request it. for the same case i use split fuction to split the index. but in this case i only use the index from `IP address` which written as array `a[1]` then parse it to new array `b[a[1]]` then iterate it to count number of request from the index. 
`split($0,a,":");b[a[1]]++;`

after that we get the number request from each IP. but we need to know which have the highest value. thereofre, we just need make algoritm as shown below: 
```
ip;
	HighestVal=0;
	for(i in b){
		if(HighestVal < b[i]){
			ip = i;
			HighestVal = b[ip]
```
lastly we need to parse it the value to a using `>` file `result.txt`
therefore final source code can be writen as : 
```
awk '{
split($0,a,":");
b[a[1]]++;}END{
	ip;
	HighestVal=0;
	for(i in b){
		if(HighestVal < b[i]){
			ip = i;
			HighestVal = b[ip]
		}
	}
print "IP yang paling banyak mengakses server adalah :" ip " sebanyak " HighestVal" request"
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
```
### Question D
In this question we asked to find how many request which use user-agent curl. the print and parse the output to a file namely `result.txt`. in this case we can use `/curl/` to find string which have name curl. then after that each finding of the curl string the value will be count as a index namely `n`. and lastly because in previous question we already fill the `result.txt` file we just need to update into the next line using `>>` so the previous data wouldnt be lose. therefore, the final source can be writen as:
```
awk '/curl/ { ++n }
END{print "ada " n " request yang menggunakan curl sebagai user-agent"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
### Question E
In this question we have to find IP address which request in 22 January at 2AM. then parse the outpot to file `result.txt`. Firtst we to find a string that consist the date and the time at 2 AM. in this case we can write it like this: 22\/Jan\/2022:02/
then we split use same fuction as before to split by `:` from each character. to know the IP address we make new index b[a[1]] and count it. the code write as:  
```
split($0,a,":")
b[a[1]]++;
count++
```
lastly we need to print the value the parse the output into a file using `>>` to file name `result.txt`
the final code file can be writen as:
```
awk '/22\/Jan\/2022:02/{
split($0,a,":")
b[a[1]]++;
count++}END{
	for(i in b){
		print i "IP Address Jam 2 Pagi"	
	}
}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
## REPORT NO.3

for question number 3 we are told to make a resource monitoring program using "Free -m". there are 4 questions, a. prompted to Enter all metrics into a log file named metrics_{YmdHms}.log; b. The script to record the above metrics is expected to run automatically every minute.; c. create a script to aggregate log files to hours; d. Because log files are sensitive, make sure all log files can only be read by the user who owns the file. 

### Question A
here we use the variable `tm` which contains `(date +%Y%m%d%H%M%s)` to be the name of our file later. the explanation contains about `Y`= years `m`= months `d`= days `H`= hours `M`= minutes `s`= seconds and at the end of the result we just call tm to name the file like `>> ~/log/metrics_$tm.log` . Then the result will be metrics_20220131150000.log(example).

### Question B
The script to record the metrics above is expected to run automatically every minute. we use `while do` and add `sleep 60` at the end to make it loop every 60 seconds/1 minute. as a result the program will continue to run every 60s until the command to stop or close.

### Question C

In the results of the aggregation file, there are minimum, maximum, and average values for each metric. in here we are using `sort` to sort the data and using `awk` to read sorted data and put in to sorted.txt. here we use an additional file to see the results of the sort that has been done. so the program task is to sort colloum by colloum in the minute_{YmdHMs}.log and print it to sorted.txt. so in the end we got all data has been sorted and last order is to print the first line and the last line to get the MAXIMUM and MINIMUM. For the everage we are using `awk -F',' '{sum1+=int($1);sum2+=int($2);sum3+=int($3);sum4+=int($4);sum5+=int($5);sum6+=int($6);sum7+=int($7);sum8+=int($8);sum9+=int($9);sum11+=int($11)}` it will read `temp.txt` which is where `temp.txt` is a copy of `minute_YmdHMs.log` so that the original file will not change. `sum1+=int($1)` will work to add up the numbers in argument1 where the location of argument 1 is the same as the column in the log. So he will add up all the data. Next printf `"average,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%s,%.1fM\` n" , sum1/(NR+.1), sum2/(NR+.1), sum3/(NR+.1), sum4/(NR+.1), sum5/(NR+.1), sum6/(NR+.1) , sum7 /(NR+.1), sum8/(NR+.1), sum9/(NR+.1), $10, sum11/(NR+.1)` will work as the divisor of the sum and will be output as the average . Last is The aggregation file will be triggered to run every hour automatically. same as question B we add sleep 3600 to make it loop every 1 hour or 3600 seconds 

### Question D

