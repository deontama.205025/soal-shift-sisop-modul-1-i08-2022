#B
awk '{split($0,a,":");
b[a[2]":"a[3]"\""]++;
}END{
	for(i in b){
	 count++;
	 sum +=b[i];}
	 sum = sum/count;
	printf "Rata-rata serangan adalah sebanyak " sum " request perjam"
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/rata-rata.txt 

#C
awk '{
split($0,a,":");
b[a[1]]++;}END{
	ip;
	HighestVal=0;
	for(i in b){
		if(HighestVal < b[i]){
			ip = i;
			HighestVal = b[ip]
		}
	}
print "IP yang paling banyak mengakses server adalah :" ip " sebanyak " HighestVal" request"
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

#D
awk '/curl/ { ++n }
END{print "ada " n " request yang menggunakan curl sebagai user-agent"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

#E
awk '/22\/Jan\/2022:02/{
split($0,a,":")
b[a[1]]++;
count++}END{
	for(i in b){
		print i "IP Address Jam 2 Pagi"	
	}
}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
