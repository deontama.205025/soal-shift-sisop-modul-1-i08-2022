#!/bin/bash/
while true
do
tm=$(date +%H)
tm=$(($tm-1))

files_arr=($(ls log | awk -v var="$tm" '{if(int(substr($1, 17, 2))==var) print($1)}'))

touch avg.txt

for file in "${files_arr[@]}"
do
    awk 'NR==2{printf "%s,0\n", $1}' "log/$file" >> avg.txt
done

touch sorted.txt

cek_st=-1
awk '{print}' temp.txt | sort -n -t ',' -k1 | awk -F',' -v var="cek_st" '{
    if (var!=$1)
        n++
        var=$1
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12  
}' | sort -n -t ',' -k2 | awk -F',' -v var="$cek_st" '{
    if (var!=$2)
        n++
        var=$2
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12  
}' | sort -n -t ',' -k3 | awk -F',' -v var="$cek_st" '{
    if (var!=$3)
        n++
        var=$3
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12  
}' | sort -n -t ',' -k4 | awk -F',' -v var="$cek_st" '{
    if (var!=$4)
        n++
        var=$4
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12  
}' | sort -n -t ',' -k5 | awk -F',' -v var="$cek_st" '{
    if (var!=$5)
        n++
        var=$5
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12  
}' | sort -n -t ',' -k6 | awk -F',' -v var="$cek_st" '{
    if (var!=$6)
        n++
        var=$6
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
}' | sort -n -t ',' -k7 | awk -F',' -v var="$cek_st" '{
    if (var!=$7)
        n++
        var=$7
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
}' | sort -n -t ',' -k8 | awk -F',' -v var="$cek_st" '{
    if (var!=$8)
        n++
        var=$8
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
}' | sort -n -t ',' -k9 | awk -F',' -v var="$cek_st" '{
    if (var!=$9)
        n++
        var=$9
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
}' | sort -n -t ',' -k11 | awk -F',' -v var="$cek_st" '{
    if (var!=$11)
        n++
        var=$11
    fi
    $12=int($12)+n
    printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
}' | sort -r -n -t ',' -k12 | awk -F',' '{
        printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
}' >> sorted.txt

tm2=$(date +%Y%m%d%H)

touch metrics_agg_$tm2.txt
awk 'END{printf "minimum,%s\n", $1}' sorted.txt >> metrics_agg_$tm2.txt
awk 'NR==1{printf "maksimum,%s\n", $1}' sorted.txt >> metrics_agg_$tm2.txt
awk '{print substr($1, 1, length($1)-1)}' avg.txt | awk -F',' '{sum1+=int($1);sum2+=int($2);sum3+=int($3);sum4+=int($4);sum5+=int($5);sum6+=int($6);sum7+=int($7);sum8+=int($8);sum9+=int($9);sum11+=int($11)} END {printf "average,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f,%s,%.1fM\n", sum1/(NR+.1), sum2/(NR+.1), sum3/(NR+.1), sum4/(NR+.1), sum5/(NR+.1), sum6/(NR+.1), sum7/(NR+.1), sum8/(NR+.1), sum9/(NR+.1), $10, sum11/(NR+.1)}' >> metrics_agg_$tm2.txt

rm sorted.txt
rm avg.txt

sleep 3600
done
